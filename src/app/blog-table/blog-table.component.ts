import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-blog-table',
  templateUrl: './blog-table.component.html',
  styleUrls: ['./blog-table.component.css']
})
export class BlogTableComponent implements OnInit {

  @Input() listBlog:any;
  @Output() onDelete = new EventEmitter()
  constructor() {
   }
  ngOnInit(): void {
  }


  delete(id: any) {
   this.onDelete.emit(id)
  }

  edit(id: any) {

  }
}
