export var positions = [
  {
    name: "Việt Nam",
    id: 1
  },
  {
    name: "Châu Á",
    id: 2
  },
  {
    name: "Châu Âu",
    id: 3
  },
  {
    name: "Châu Mỹ",
    id: 4
  },
]

export var categories = [
  { name: 'Tổng hợp', id: 0},
  { name: 'Thời sự', id: 1},
  { name: 'Thời trang', id: 2},
  { name: 'Thể thao', id: 3},
  { name: 'Thời tiết', id: 4},
  { name: 'Khoa học', id: 6},
  { name: 'Điện ảnh', id: 7},
  { name: 'âm nhạc', id: 8},
  { name: 'kinh tế', id: 9},
]
