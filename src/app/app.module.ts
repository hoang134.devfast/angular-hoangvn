import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { BlogTableComponent } from './blog-table/blog-table.component';
import { BlogFormSearchComponent } from './blog-form-search/blog-form-search.component';
import { BlogFormCreateEditComponent } from './blog-form-create-edit/blog-form-create-edit.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { PageIndexComponent } from './page-index/page-index.component';
import { BlogModalComponent } from './blog-modal/blog-modal.component';
import { BtableTestComponent } from './btable-test/btable-test.component';
import { CountriesComponent } from './countries/countries.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    BlogTableComponent,
    BlogFormSearchComponent,
    BlogFormCreateEditComponent,
    FooterComponent,
    PageIndexComponent,
    BlogModalComponent,
    BtableTestComponent,
    CountriesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
