
import { Component, OnInit } from '@angular/core';
import { blog } from '../model/blog.model';
import { ApiService } from '../api-service/api.service';
import { ActivatedRoute } from '@angular/router';
import {Router} from "@angular/router"
import {positions, categories} from "../constant/constant"

@Component({
  selector: 'app-blog-form-create-edit',
  templateUrl: './blog-form-create-edit.component.html',
  styleUrls: ['./blog-form-create-edit.component.css']
})
export class BlogFormCreateEditComponent implements OnInit {
  public blog : any
  public id: number | undefined
  public positions = positions
  public categories = categories
  constructor(private api: ApiService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(
      params => {
        this.id = params['id']
      })
  }

  ngOnInit(): void {
    this.blog = new blog()
    if(this.id != undefined) {
      this.getBlog(this.id)
    }
    
  }

  onSubmit() {
    if(this.id != undefined) {
      this.api.updateBlog(this.id, this.blog).subscribe(data => {
      })
    }
    else {
      this.api.createBlog(this.blog).subscribe(data => {
      })
    }
    this.router.navigate(['/'])
  }

  clearForm() {
   this.blog = new blog()
  }

  getBlog(id: number) {
    this.api.getBlog(id).subscribe(data => {
      this.blog = data

    })
  }
}
