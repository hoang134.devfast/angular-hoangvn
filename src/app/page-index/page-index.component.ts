import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api-service/api.service';

@Component({
  selector: 'app-page-index',
  templateUrl: './page-index.component.html',
  styleUrls: ['./page-index.component.css']
})
export class PageIndexComponent implements OnInit {
  public listBlog: any;
  title = 'blog';
  public setListBlog(listBlog: any) {
    console.log('data', listBlog)
    this.listBlog = listBlog
  }
  constructor(private api: ApiService) {
  }

  getListBlog() {
    this.api.getListBlog().subscribe(data => {
      this.setListBlog(data)
    })
  }

  deleteBlog(id: any) {
    this.api.deleteBlog(id).subscribe(data => {
      this.getListBlog()
    })
  }

  searchBlog(title: string) {
    this.api.searchBlog(title).subscribe(data => {
      this.setListBlog(data)
    })
  }

  ngOnInit() {
    this.getListBlog()
  }

}
