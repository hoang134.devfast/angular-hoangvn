import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormsModule }   from '@angular/forms';
@Component({
  selector: 'app-blog-form-search',
  templateUrl: './blog-form-search.component.html',
  styleUrls: ['./blog-form-search.component.css']
})
export class BlogFormSearchComponent implements OnInit {
  public title: any
  @Output() onSearch = new EventEmitter()
  constructor() {
  }

  ngOnInit(): void {
  }

  search() {
    this.onSearch.emit(this.title)
  }
}
