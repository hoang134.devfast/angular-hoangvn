import { blog } from '../model/blog.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const localUrl = 'http://localhost:3000/blogs/';
@Injectable({
  providedIn: 'root'
})

export class ApiService {

  constructor(private http: HttpClient) { }

  getListBlog() {
    return this.http.get(localUrl);
  }

  deleteBlog(id: number) {
    return this.http.delete(localUrl + id);
  }

  searchBlog(title: string) {
    return this.http.get(localUrl + '?title_like=' + title);
  }

  createBlog(blog: blog) {
    return this.http.post(localUrl, blog);
  }

  updateBlog(id: number, blog: blog) {
    return this.http.put(localUrl + id, blog);
  }

  getBlog(id: number) {
    return this.http.get(localUrl + id);
  }
}
