import { PageIndexComponent } from './page-index/page-index.component';
import { BlogFormCreateEditComponent } from './blog-form-create-edit/blog-form-create-edit.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: PageIndexComponent },
  { path: 'create', component: BlogFormCreateEditComponent },
  { path: 'edit/:id', component: BlogFormCreateEditComponent }
];

@NgModule({
  declarations: [],
  imports: [
    [RouterModule.forRoot(routes)],
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
