import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BtableTestComponent } from './btable-test.component';

describe('BtableTestComponent', () => {
  let component: BtableTestComponent;
  let fixture: ComponentFixture<BtableTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BtableTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BtableTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
